import requests
from PyQt5.Qt import QObject
from PyQt5.QtCore import QRunnable, pyqtSignal


class WorkerSignals(QObject):
    result = pyqtSignal()


class HeartBeat(QRunnable):
    def __init__(self, caller, server_details, user_details, sync_results):
        QRunnable.__init__(self)
        self.signals = WorkerSignals()
        self.caller = caller
        self.server_address, self.class_key, self.ssl_set = server_details
        self.first_name, self.surname, self.email = user_details
        self.sync_results = sync_results
        self.sync_results['successful'] = True

    def run(self):
        if self.ssl_set:
            protocol = "https"
            port = "443"
        else:
            protocol = "http"
            port = "80"
            # for Dev only
            # port = "9980"

        request_str = "{}://{}:{}/api/heartbeat?classcode={}".format(
            protocol,
            self.server_address,
            port,
            self.class_key
        )

        # print(request_str)
        try:
            request_data = requests.get(request_str)
            request_data.raise_for_status()
        except requests.HTTPError as http_err:
            self.sync_results['successful'] = False
            print('HTTP ERROR: {http_err}')
            self.call_return()
            return
        except Exception as err:
            self.sync_results['successful'] = False
            print('Error: {err}')
            self.call_return()
            return

        if request_data.status_code != 200:
            self.sync_results['successful'] = False

        self.call_return()

    def call_return(self):
        # print("Call Returning")
        self.signals.result.emit()
